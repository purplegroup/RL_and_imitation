import pdb
import numpy
from PIL import Image, ImageDraw
import numpy as np
import cv2
import matplotlib.pyplot as plt
#from __future__ import print_function    # (at top of module)
import math

class VirtualSegmentation(object):
    """A virtual simulation environment using segmentation as input to the robot contols
    """

    def __init__(self, environment, forward = 1, theta=1, ROI = [180, 240, 0, 240]):
        
        self.environment = environment
        self.forward = forward   # Units by which to move the robot forward
        self.theta = theta       # Angle in degrees by which to turn right or left
        self.dest_pts = np.float32([[0, 0], [240, 0], [0, 240], [240, 240]])
        self.orig_pts = np.float32([[50+100, 200+300],[400+100,200+300],[160+100,500+300],  [320+100,500+300]])
        self.arena_size = self.environment.shape[0]
        self.done = False
        
        #self.stopping_region =  np.zeros(tuple(self.dest_pts[3]))
        #self.stopping_region[190:230, 80:160] = 1
        
        self.ROI = ROI
        
        
        self.get_robot_view()
        self.get_mask_arena()
        self.display = False
        self.numPixels = 0
        if len(np.where(self.mask_arena == 127)[0])>1:
            self.numPixels = len(np.where(self.observation == 1)[0])
        self.action_to_num = {"left":0, "forward":1, "right":2}
        self.num_to_action = {0:"left", 1:"forward", 2:"right"}
        cv2.destroyAllWindows()

    def reset(self):
        self.done = False
        offset = np.random.randint(100,700)
        
        self.orig_pts = np.float32([[50+300, 200+300],[400+300,200+300],[200+300,500+300],  [280+300,500+300]])
        #self.orig_pts = np.float32([[50+offset, 200+300],[400+offset,200+300],[160+offset,500+300],  [320+offset,500+300]])
        self.get_robot_view()
        self.numPixels = 0
        if len(np.where(self.mask_arena == 127)[0])>1:
            self.numPixels = len(np.where(self.observation == 1)[0])
            
        return self.observation
    
    def render(self):
        self.display = True
    
    def step(self,action):
        if action ==0:
            self.move_left()
            
        if action ==1:
            self.move_forward()

        if action ==2:
            self.move_right()
        
        self.get_robot_view()
        
        
        
        
        
        if self.display:
            self.display_images()
            
        return self.observation, self.get_reward(), self.done
            
    def get_reward(self):
        reward = -2
        
        ROI = self.observation[self.ROI[0]:self.ROI[1], self.ROI[2]:self.ROI[3]]
        
        allpixels = (self.ROI[3] - self.ROI[2]) * (self.ROI[1] - self.ROI[0])
        
        #print(len(np.where(ROI == 1)[0]))
        newNumPixels = 0
        if len(np.where(self.mask_arena == 127)[0])>1:
            newNumPixels = len(np.where(self.observation == 1)[0])
            
        if (newNumPixels>10) and (newNumPixels > 1.05 * self.numPixels):
            reward = 1
        
        if (len(np.where(ROI == 1)[0])> allpixels * 0.05):
            reward = 500
            self.done = True
            
        if (len(np.where(ROI == 0)[0])> allpixels * 0.25):
            reward = -500
            self.done = True
        
        self.numPixels = newNumPixels
            

            
        return reward
            
        
        
    def move_forward(self):
        vector = self.orig_pts[3] - self.orig_pts[2]
        perp = np.flip(vector,0)/ np.linalg.norm(vector) * self.forward
        perp[1] = perp[1] * -1
        self.orig_pts = self.orig_pts + perp
        
        
        

    def move_right(self):
        self.orig_pts = self.rotatePolygon(self.theta)

    def move_left(self):
        self.orig_pts = self.rotatePolygon(-self.theta)
        
    def get_robot_view(self):
        M = cv2.getPerspectiveTransform(self.orig_pts, self.dest_pts)
        self.observation = np.round(cv2.warpPerspective(self.environment, M, tuple(self.dest_pts[3])))
        #self.observation[self.observation<1] = 0
        #self.observation[self.observation>1] = 2
        
        
    def get_mask_arena(self):
        
        polygon = self.orig_pts
        polygon = (polygon).astype("int")
        polygon[[0, 1, 2, 3]] = polygon[[0, 1, 3, 2]]
        polygon = list([tuple(row) for row in polygon])

        width = self.arena_size
        height = self.arena_size
        
        center = int((self.arena_size)/2)
        
        # https://stackoverflow.com/questions/3654289/scipy-create-2d-polygon-mask
        img = Image.new('L', (width, height), 0)
        ImageDraw.Draw(img).polygon(polygon, outline=1, fill=1)
        mask = numpy.array(img)
        self.mask_arena = mask*self.environment
        self.mask_arena = (self.mask_arena/np.max(self.mask_arena) * 255).astype("uint8")

    def draw_arena_mask(self):
        self.get_mask_arena()
        scaling = 0.6
        center = int((self.arena_size)/2)
        #cv2.imshow("arena",mask_arena )
   
        cv2.imshow("arena", self.mask_arena[int(center - scaling*center):int(center + scaling*center), int(center - scaling*center):int(center + scaling*center)])
        #plt.imshow(mask_arena)
        #plt.show()
        
    def rotatePolygon(self, ClockwiseRotationAngle):
        #https://stackoverflow.com/questions/20023209/function-for-rotating-2d-objects
        """Rotates the given polygon which consists of corners represented as (x,y),
        around the ORIGIN, clock-wise, theta degrees"""
        theta = math.radians(ClockwiseRotationAngle)
        polygon = self.orig_pts
        center = np.mean(polygon[2:, :], axis =0)
        polygon = (polygon - center).tolist()    
        rotatedPolygon = []
        for corner in polygon :
            rotatedPolygon.append(( corner[0]*math.cos(theta)-corner[1]*math.sin(theta) , corner[0]*math.sin(theta)+corner[1]*math.cos(theta)) )

        rotatedPolygon = np.array(rotatedPolygon) + center
        return rotatedPolygon.astype("float32")
    

        
    def display_images(self):
        self.draw_arena_mask()
        self.get_robot_view()
        cv2.imshow("Robot view",(self.observation/np.max(self.observation)*255).astype("uint8"))
        cv2.waitKey(10)
        #plt.show()
